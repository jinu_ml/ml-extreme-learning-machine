# CONTEXTUAL BASED ANOMALY DETECTION USING EXTREME LEARNING MACHINE AUTOENCODERS (ELM).

An contextual based anomaly detection model using Extreme Learning Machine (L2-ELMAD) autoencoder and Extreme Learning Machine (Em-ELMAD) embedding methods for flight anomaly detection. These methods were faster to use compared to One-Class Support Vector Machine (OSVM) and Multi-Kernal Anomaly detection methods traditionally employed for anomaly detection in Aerospace Domain. 

### Its an implementation of work from following research papers-


<ol>
<li>[Semi-Supervised and Unsupervised Extreme Learning Machines-- Gao Huang, Shiji Song, Jatinder N. D. Gupta, and Cheng Wu](https://ieeexplore.ieee.org/document/7727444)</li>
<li>[Anomaly detection in aviation data using extreme learning machines(NASA)- Janakiraman, Vijay Manikandan and Nielsen, David](https://ieeexplore.ieee.org/document/7727444)</li>
</ol> 

